package lk.ijse.student_crud.service;

import lk.ijse.student_crud.dto.StudentDTO;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

public interface StudentService {
    void saveStudent(StudentDTO studentDTO);

    void updateStudent(String id, StudentDTO studentDTO);

    void deleteStudent(String id);

    StudentDTO findStudent(String id);

    List<StudentDTO> findAllStudent();
}
