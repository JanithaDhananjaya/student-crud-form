package lk.ijse.student_crud.service.impl;

import lk.ijse.student_crud.dto.StudentDTO;
import lk.ijse.student_crud.entity.Student;
import lk.ijse.student_crud.repositry.StudentRepositry;
import lk.ijse.student_crud.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepositry studentRepositry;

    @Override
    public void saveStudent(StudentDTO studentDTO) {
        studentRepositry.save(
                new Student(studentDTO.getStudentid(),
                        studentDTO.getStudentname(), studentDTO.getAddress(), studentDTO.getMobileno()));
    }

    @Override
    public void updateStudent(String id, StudentDTO studentDTO) {
        if (!studentDTO.getStudentid().equals(id)) {
            throw new RuntimeException("Student ID mismatched");
        }
        studentRepositry.save(new Student(studentDTO.getStudentid(),studentDTO.getStudentname(), studentDTO.getAddress(), studentDTO.getMobileno()));
    }

    @Override
    public void deleteStudent(String id) {
        studentRepositry.deleteById(id);
    }

    @Override
    public StudentDTO findStudent(String id) {
        Student student = studentRepositry.findById(id).get();
        StudentDTO studentDTO = new StudentDTO(student.getStudentid(), student.getStudentname(), student.getAddress(), student.getMobileno());
        return studentDTO;
    }

    @Override
    public List<StudentDTO> findAllStudent() {
        ArrayList<StudentDTO> allStudents = new ArrayList<>();
        List<Student> all = (List<Student>) studentRepositry.findAll();
        for (Student student : all) {
            StudentDTO studentDTO = new StudentDTO(student.getStudentid(), student.getStudentname(), student.getAddress(), student.getMobileno());
            allStudents.add(studentDTO);
        }
        return allStudents;
    }
}
