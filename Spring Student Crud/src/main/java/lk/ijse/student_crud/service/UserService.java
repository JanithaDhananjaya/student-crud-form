package lk.ijse.student_crud.service;

import lk.ijse.student_crud.dto.UserDTO;

import java.util.List;

public interface UserService {
    List<UserDTO> getUserDetails();
}
