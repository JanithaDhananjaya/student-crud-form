package lk.ijse.student_crud.service.impl;

import lk.ijse.student_crud.dto.UserDTO;
import lk.ijse.student_crud.entity.User;
import lk.ijse.student_crud.repositry.UserRepositry;
import lk.ijse.student_crud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepositry userRepositry;

    @Override
    public List<UserDTO> getUserDetails() {
        List<User> user = (List<User>) userRepositry.findAll();
        ArrayList<UserDTO> userlist = new ArrayList<>();
        for (User us : user) {
            userlist.add(new UserDTO(us.getUserid(), us.getUsername(), us.getPassword()));
        }
        return userlist;
    }

}
