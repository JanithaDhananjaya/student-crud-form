package lk.ijse.student_crud.main;

import lk.ijse.student_crud.repositry.StudentRepositry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @Autowired
    private StudentRepositry studentRepositry;

    @GetMapping("/hello")
    public String init(){
        return "index";
    }
//    public ModelAndView goHome(){
//        ModelAndView modelAndView=new ModelAndView("index");
//        modelAndView.addObject("lists",studentRepositry.findAll());
//        return modelAndView;
//    }

}
