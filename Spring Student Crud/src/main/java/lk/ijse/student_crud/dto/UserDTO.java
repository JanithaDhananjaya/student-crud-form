package lk.ijse.student_crud.dto;

public class UserDTO {
    private String userid;
    private String username;
    private String password;


    public UserDTO() {
    }

    public UserDTO(String userid, String username, String password) {
        this.setUserid(userid);
        this.setUsername(username);
        this.setPassword(password);
    }


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
