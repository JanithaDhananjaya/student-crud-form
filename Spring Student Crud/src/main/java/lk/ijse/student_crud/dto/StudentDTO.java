package lk.ijse.student_crud.dto;

public class StudentDTO {

    private String studentid;
    private String studentname;
    private String address;
    private String mobileno;

    public StudentDTO() {
    }

    public StudentDTO(String studentid, String studentname, String address, String mobileno) {
        this.setStudentid(studentid);
        this.setStudentname(studentname);
        this.setAddress(address);
        this.setMobileno(mobileno);
    }


    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public String getStudentname() {
        return studentname;
    }

    public void setStudentname(String studentname) {
        this.studentname = studentname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }
}
