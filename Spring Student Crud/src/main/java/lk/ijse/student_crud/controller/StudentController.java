package lk.ijse.student_crud.controller;

import lk.ijse.student_crud.dto.StudentDTO;
import lk.ijse.student_crud.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/v1/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveStudent(@RequestBody StudentDTO studentDTO) {
        studentService.saveStudent(studentDTO);
    }

    @PostMapping(value = "{studentid}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateStudent(@PathVariable("studentid") String id, @RequestBody StudentDTO studentDTO) {
        studentService.updateStudent(id, studentDTO);
    }

    @DeleteMapping(value = "{studentid}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteStudent(@PathVariable("studentid") String id) {
        studentService.deleteStudent(id);
    }

    @GetMapping(value = "{studentid}",produces = MediaType.APPLICATION_JSON_VALUE)
    public StudentDTO searchStudent(@PathVariable("studentid") String id) {
        return studentService.findStudent(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<StudentDTO> findAllStudents() {
        return studentService.findAllStudent();
    }
}
