package lk.ijse.student_crud.repositry;

import lk.ijse.student_crud.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepositry extends CrudRepository<User,String> {
}
