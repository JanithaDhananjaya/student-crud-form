package lk.ijse.student_crud.repositry;

import lk.ijse.student_crud.entity.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepositry extends CrudRepository<Student, String> {
}
